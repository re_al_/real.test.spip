<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/minibando/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_minibando' => 'Paramètres',

	// L
	'label_disposition' => 'Disposition',
	'label_disposition_horizontale' => 'Horizontale',
	'label_disposition_verticale' => 'Verticale',
	'label_limite' => 'Limiter',
	'label_limite_webmestre' => 'Afficher le minibando uniquement pour les webmestres',

	// M
	'mode_css' => 'Mode css',
	'mode_debug' => 'Mode debug',
	'mode_inclure' => 'Mode inclure',
	'mode_profile' => 'Mode profile',
	'mode_traduction' => 'Mode traduction',

	// T
	'titre_debug' => 'Debug',
	'titre_outils_rapides' => 'Création',
	'titre_page_configurer_minibando' => 'Configurer le minibando'
);
