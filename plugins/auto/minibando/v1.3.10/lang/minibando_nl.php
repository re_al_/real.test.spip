<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/minibando?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_minibando' => 'Parameters',

	// L
	'label_disposition' => 'Plaaatsing',
	'label_disposition_horizontale' => 'Horizontaal',
	'label_disposition_verticale' => 'Verticaal',
	'label_limite' => 'Beperken',
	'label_limite_webmestre' => 'Toon de minibando uitsluitend voor webmasters',

	// M
	'mode_css' => 'CSS modus',
	'mode_debug' => 'Debug modus',
	'mode_inclure' => 'Inclure modus',
	'mode_profile' => 'Profiel modus',
	'mode_traduction' => 'Vertaalmodus',

	// T
	'titre_debug' => 'Debug',
	'titre_outils_rapides' => 'Aanmaken',
	'titre_page_configurer_minibando' => 'Minibando configureren'
);
