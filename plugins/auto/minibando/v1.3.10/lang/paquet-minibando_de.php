<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-minibando?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'minibando_description' => 'Das Plugin Minibando ersetzt die Reiter zur Verwaltung von SPIP und seinen Objekten durch eine Werkzeugleiste mit den wichtigsten Elementen des Redaktionssystems.',
	'minibando_slogan' => 'Minibando bringt maximale Funktion!'
);
