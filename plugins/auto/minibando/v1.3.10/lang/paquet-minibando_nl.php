<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-minibando?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'minibando_description' => 'De plugin minibando vervangt de beheerknoppen van SPIP door een knoppenbalk met alle elementen die zich in de menubalk van het privé gedeelte van SPIP bevinden.',
	'minibando_slogan' => 'Minibando, een knoppenbalk met maximale functionaliteit!'
);
