<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/public?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
    'real_titulo_1' => 'Last news',
	'real_titulo_2' => 'Polítics',
	'real_titulo_3' => 'Archive',
	'real_titulo_4' => 'Etiquetas:',
	'real_titulo_5' => 'Sin etiquetas:',
);
